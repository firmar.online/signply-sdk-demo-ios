//
//  ContentView.swift
//  SignplySdkDemo
//
//  Created by Movilidad Edatalia on 24/10/23.
//

import SwiftUI
import SignplySDK

struct ContentView: View {
    
    @StateObject private var signplySdkParams: ObservableSignplySdkParams = ObservableSignplySdkParams(initialParams: SignplySDKParams())
    @State private var isPickingDocument = false
    @State private var isPickingCertificate = false
    @State private var isShowingAlert = false
    @State private var messageAlert = ""
    @State private var color = Color.black
    @State private var isShowingLimitAlert = false
    
    private let widgetManualRatioRange: ClosedRange<Float> = 1...4
    private let widgetManualRatioStep = 0.1
    
    private let widgetCustomTextRange: ClosedRange<Int> = 1...15
    private let widgetCustomTextStep = 1
    
    private let signatureThicknessRange: ClosedRange<Int> = 5...25
    private let signatureThicknessStep = 5
    
    
    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("document_properties")) {
                    
                    Button(action: {
                        isPickingDocument = true
                    }, label: {
                        Text("pick_document")
                    })
                    
                    if signplySdkParams.wrappedValue.fileProperties.documentPath != "" {
                        Text(URL(fileURLWithPath: signplySdkParams.wrappedValue.fileProperties.documentPath).lastPathComponent).font(.callout).offset(y: -8)
                    } else {
                        Text("default_document").font(.callout)
                            .offset(y: -8)
                    }
                    
                    Toggle("show_reject", isOn: $signplySdkParams.wrappedValue.fileProperties.isShareable)
                    
                    
                    DisclosureGroup("advanced") {
                        VStack {
                            TextField("signed_document_name", text: $signplySdkParams.wrappedValue.fileProperties.signedName){}.textFieldStyle(.roundedBorder)
                            SecureField("password", text: $signplySdkParams.wrappedValue.fileProperties.password){}.textFieldStyle(.roundedBorder)
                            TextField("author", text: $signplySdkParams.wrappedValue.fileProperties.author) {}  .textFieldStyle(.roundedBorder)
                            TextField("reason", text: $signplySdkParams.wrappedValue.fileProperties.reason) {}  .textFieldStyle(.roundedBorder)
                            TextField("contact", text: $signplySdkParams.wrappedValue.fileProperties.contact) {}  .textFieldStyle(.roundedBorder)
                            TextField("location", text: $signplySdkParams.wrappedValue.fileProperties.location) {}  .textFieldStyle(.roundedBorder)
                        }
                    }
                    
                }.listRowSeparator(.hidden)
                    .fileImporter(isPresented: $isPickingDocument,
                                  allowedContentTypes: [.pdf]) { result in
                        let url = try? result.get()
                        url?.startAccessingSecurityScopedResource()
                        signplySdkParams.wrappedValue.fileProperties.documentPath = url?.path ?? ""
                        isPickingDocument = false
                    }
                
                Section(header: Text("common_properties")) {
                    Toggle("sign_all_pages", isOn: $signplySdkParams.wrappedValue.widget.signOnAllPages)
                    TextField("title", text: $signplySdkParams.wrappedValue.commonProperties.title.toUnwrapped(defaultValue: "")) {}.textFieldStyle(.roundedBorder)
                    Toggle("require_gps", isOn: $signplySdkParams.wrappedValue.commonProperties.requestLocation)
                    Toggle("private_folder", isOn: $signplySdkParams.wrappedValue.commonProperties.saveOnPrivateStorage)
                    
                }.listRowSeparator(.hidden)
                
                Section(header: Text("signature_box")) {
                    Picker("positioning_type", selection: $signplySdkParams.wrappedValue.widget.widgetType) {
                        ForEach(SignplySDKWidgetType.allCases) { option in
                            Text(String(describing: option))
                        }
                    }
                    if signplySdkParams.wrappedValue.widget.widgetType == .Manual {
                        Stepper(value: $signplySdkParams.wrappedValue.widget.widgetManualRatio,
                                in: widgetManualRatioRange,
                                step: Float.Stride(widgetManualRatioStep)) {
                            HStack {
                                Text("aspect_ratio")
                                Text(String(format: "%.1f", round(signplySdkParams.wrappedValue.widget.widgetManualRatio * 10) / 10.0))
                            }
                        }
                    }
                    if signplySdkParams.wrappedValue.widget.widgetType == .Field {
                        TextField("field_annotation_name", text: $signplySdkParams.wrappedValue.widget.widgetFieldFieldname.toUnwrapped(defaultValue: "")){}.textFieldStyle(.roundedBorder)
                    }
                    
                    if signplySdkParams.wrappedValue.widget.widgetType == .Fixed {
                        
                        TextField("page", value: $signplySdkParams.wrappedValue.widget.widgetFixedPage, format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                        TextField("width", value: $signplySdkParams.wrappedValue.widget.widgetWidth, format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                        TextField("height", value: $signplySdkParams.wrappedValue.widget.widgetHeight, format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                        TextField("position_x", value: $signplySdkParams.wrappedValue.widget.widgetFixedX, format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                        TextField("position_y", value: $signplySdkParams.wrappedValue.widget.widgetFixedY, format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                        
                        
                    }
                    
                    if signplySdkParams.wrappedValue.widget.widgetType == .Float {
                        
                        TextField("text_to_find", text: $signplySdkParams.wrappedValue.widget.widgetFloatText.toUnwrapped(defaultValue: "")) {}
                            .textFieldStyle(.roundedBorder)
                        
                        TextField("gap_x", value: $signplySdkParams.wrappedValue.widget.widgetFloatGapX.toUnwrapped(defaultValue: 0), format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                        TextField("gap_y", value: $signplySdkParams.wrappedValue.widget.widgetFloatGapY.toUnwrapped(defaultValue: 0), format: .number)
                            .textFieldStyle(.roundedBorder)
                        
                    }
                    
                }.listRowSeparator(.hidden)
                
                Section(header: Text("custom_text")) {
                    
                    Toggle("require_custom_text", isOn: $signplySdkParams.wrappedValue.widget.requestWidgetCustomText)
                    
                    Button("add_text_row") {
                        withAnimation {
                            signplySdkParams.addCustomText(signplySdkWidgetCustomText: SignplySDKWidgetCustomText())
                        }
                    }
                    
                    List {
                        ForEach(signplySdkParams.wrappedValue.widget.widgetCustomText.indices, id: \.self) { index in
                            
                            HStack {
                                TextField("text", text: $signplySdkParams.wrappedValue.widget.widgetCustomText[index].text)
                                    .textFieldStyle(.roundedBorder)
                                
                                Stepper(value: $signplySdkParams.wrappedValue.widget.widgetCustomText[index].fontSize,
                                        in: widgetCustomTextRange,
                                        step: widgetCustomTextStep) {
                                    
                                }
                                Text(String(signplySdkParams.wrappedValue.widget.widgetCustomText[index].fontSize))
                                
                            }
                        }.onDelete { indexSet in
                            withAnimation {
                                signplySdkParams.removeCustomText(offsets: indexSet)
                            }
                        }
                    }
                    
                }.listRowSeparator(.hidden)
                
                
                Section(header: Text("extra_properties")) {
                    
                    ColorPicker("signature_color", selection: $color, supportsOpacity: false)
                    
                    Stepper(value: $signplySdkParams.wrappedValue.extra.signatureThickness,
                            in: signatureThicknessRange,
                            step: signatureThicknessStep) {
                        
                        HStack {
                            Text("signature_thickness")
                            
                            Text(
                                "\(signplySdkParams.wrappedValue.extra.signatureThickness)")
                        }
                        
                    }
                    
                    Toggle("open_signature_drawer_auto", isOn: $signplySdkParams.wrappedValue.extra.autoOpen)
                    
                    Toggle("all_pages_before_sign", isOn: $signplySdkParams.wrappedValue.extra.viewLastPage)
                    
                    Toggle("show_reject_button", isOn: $signplySdkParams.wrappedValue.extra.showReject)
                    
                    Toggle("full_screen", isOn: $signplySdkParams.wrappedValue.extra.fullScreen.toUnwrapped(defaultValue: false))
                    
                    
                }.listRowSeparator(.hidden)
                
                Section(header: Text("signature_security")) {
                    
                    Toggle("timestamp", isOn: $signplySdkParams.wrappedValue.tsp.tspActivate)
                    
                    TextField("url", text: $signplySdkParams.wrappedValue.tsp.tspURL)
                        .textFieldStyle(.roundedBorder)
                    
                    TextField("user", text: $signplySdkParams.wrappedValue.tsp.tspUser.toUnwrapped(defaultValue: ""))
                        .textFieldStyle(.roundedBorder)
                    
                    SecureField("password", text: $signplySdkParams.wrappedValue.tsp.tspPassword.toUnwrapped(defaultValue: ""))
                        .textFieldStyle(.roundedBorder)
                    
                    Toggle("long_term_signature", isOn: $signplySdkParams.wrappedValue.certificate.ltv)
                    
                    
                }.listRowSeparator(.hidden)
                
                Section(header: Text("signature_certificate")) {
                    
                    Button(action: {
                        isPickingCertificate = true
                    }, label: {
                        Text("select_certificate")
                    })
                    
                    if signplySdkParams.wrappedValue.certificate.signCertP12B64 != nil {
                        
                        List {
                            ForEach(0..<1) { index in
                                Text(signplySdkParams.wrappedValue.certificate.signCertP12B64 ?? "").lineLimit(1).font(.callout).offset(y: -8)
                            }.onDelete { _ in
                                withAnimation {
                                    signplySdkParams.wrappedValue.certificate.signCertP12B64 = nil
                                }
                            }
                        }
                        
                    } else {
                        Text("default_certificate").font(.callout).offset(y: -8)
                    }
                    
                    SecureField("password", text: $signplySdkParams.wrappedValue.certificate.signCertPassword.toUnwrapped(defaultValue: ""))
                        .textFieldStyle(.roundedBorder).offset(y: -8)
                    
                    
                }.listRowSeparator(.hidden)
                    .fileImporter(isPresented: $isPickingCertificate,
                                  allowedContentTypes: [.item]) { result in
                        
                        if let url = try? result.get(), let data = try? Data(contentsOf: url) {
                            withAnimation {
                                signplySdkParams.wrappedValue.certificate.signCertP12B64 = data.base64EncodedString()
                                
                            }
                        }
                    }
            }
            .navigationTitle("functional_sign")
            .toolbar {
                
                ToolbarItem(placement: .navigationBarLeading) {
                    Text("v\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")")
                }
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    
                    Button("sign") {
                        signDocument()
                    }
                }
                
            }.alert(messageAlert, isPresented: $isShowingAlert) {
                Button("ok", role: .cancel) {}
            }
        }.navigationViewStyle(StackNavigationViewStyle())
            .onAppear {
                if signplySdkParams.wrappedValue.licenseB64.isEmpty,
                   let licenseResource =  Bundle.main.url(forResource: "license", withExtension: "lic"),
                   let licenseB64 = try? String(contentsOf: licenseResource, encoding: .utf8){
                    signplySdkParams.wrappedValue.licenseB64 = licenseB64
                }
            }.alert(isPresented: $isShowingLimitAlert) {
                Alert(
                    title: Text(""),
                    message: Text("reached_signatures_limit"),
                    primaryButton: .default(Text("contact_us")) {
                        if let url = URL(string: "https://www.signply.com/contactar?req=app_ios_demo") {
                            UIApplication.shared.open(url)
                        }
                    },
                    secondaryButton: .default(Text("go_to_signply")) {
                        if let url = URL(string: "https://apps.apple.com/us/app/firma-digital-pdf-signply/id1618906794") {
                            UIApplication.shared.open(url)
                        }
                    }
                )
            }
    }
    
    private func signDocument() {
        let signatures = UserDefaults.standard.integer(forKey: "signatures")
        if (signatures > 19) {
            isShowingLimitAlert = true
            return
        }
        
        if let vc = UIApplication.shared.windows.first?.rootViewController {
            signplySdkParams.wrappedValue.extra.signatureColorHex = color.toHexString()
            signplySdkParams.wrappedValue.commonProperties.reference = "Demo iOS"
            if signplySdkParams.wrappedValue.fileProperties.documentPath.isEmpty,
               let documentPath = Bundle.main.url(forResource: "example", withExtension: "pdf")?.path {
                signplySdkParams.wrappedValue.fileProperties.documentPath = documentPath
            }
            
            SignplySDKLauncher.handleDocument(signplySDKParams: signplySdkParams.wrappedValue, on: vc, successCallback: { url in
                
                let defaults = UserDefaults.standard
                let signatures = defaults.integer(forKey: "signatures")
                defaults.set(signatures + 1, forKey: "signatures")
                
                URL(fileURLWithPath: signplySdkParams.wrappedValue.fileProperties.documentPath)
                    .stopAccessingSecurityScopedResource()
                signplySdkParams.wrappedValue.fileProperties.documentPath = url.path
                signDocument()
            }, rejectCallback: { _ in }, errorCallback: { error in
                self.isShowingAlert = true
                self.messageAlert = error ?? "signing_error"
            })
        }
    }
}

extension Binding {
    func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }
}

extension Color {
    func toHexString() -> String {
        if self == .clear {
            return "clear"
        }
        
        let components = self.cgColor!.components!
        
        let r = Int(components[0] * 255.0)
        let g = Int(components[1] * 255.0)
        let b = Int(components[2] * 255.0)
        
        return String(format: "#%02X%02X%02X", r, g, b)
    }
}

class ObservableSignplySdkParams: ObservableObject {
    @Published var wrappedValue: SignplySDKParams
    
    init(initialParams: SignplySDKParams) {
        self.wrappedValue = initialParams
    }
    
    func addCustomText(signplySdkWidgetCustomText: SignplySDKWidgetCustomText) {
        let newParams = wrappedValue
        newParams.widget.widgetCustomText.append(signplySdkWidgetCustomText)
        self.wrappedValue = newParams
    }
    
    func removeCustomText(offsets: IndexSet) {
        let newParams = wrappedValue
        newParams.widget.widgetCustomText.remove(atOffsets: offsets)
        self.wrappedValue = newParams
    }
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

