//
//  SignplySdkDemoApp.swift
//  SignplySdkDemo
//
//  Created by Movilidad Edatalia on 24/10/23.
//

import SwiftUI

@main
struct SignplySdkDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
                .onAppear {
                    if #available(iOS 16.0, *) {
                        print(URL.documentsDirectory)
                    }
                }
        }
    }
}
