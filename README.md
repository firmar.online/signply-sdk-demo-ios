# SIGNply SDK iOS Demo

The source code for the SIGNply SDK Demo Application for iOS operating systems is published here.

This app allows you to explore the integration options and full potential of the SIGNply SDK through a native iOS app.

# IMPORTANT

To compile after downloading you must **extract** the contents of the zip SignplySB.xframework.zip on same folder.

# Functioning

The application allows you to play with several customizations to sign a document.

It is necessary to select a PDF to be able to sign. If no pdf is selected it opens one by default.

# Clarification

This application uses the SignplySDK library and SignplySB for the digital signature process that is located inside the SignplySdkDemo folder. The SignplySB is compressed, you must unzip the file. 

The signing SDK receives a few parameters, including the document url in the device. The data model is exposed below

# Permissions

* Location: Used to add the geolocation to the signature if required.

* Network connection: Used in case you want to make a signature with a time stamp.


# Requirements

Operating system: iOs 11.0 or higher. Compatible with Objetive C.

# Compilation

It can be compiled with Xcode -> Run

# Architecture

The application uses the architecture pattern MVC with Storyboard Scene

# License

The SIGNply iOS SDK needs a license to work, which in this case is included in the Demo application that uses it (SIGNplyExternal/Resources).

** This license is only valid for testing and should never be used in production.**

# Instantiation

The minimun instantiation process:

1 -> Declare a signplySDKParams

```swift
    let signplySDKParams = SignplySDKParams()
    let signplySDKParams = SignplySDKParams(licenseB64: "license in base64",
                                            fileProperties: SignplySDKFileProperties(documentPath: "document url path"))
```

2 -> Launch a launcher

```swift
    SignplySDKLauncher.handleDocument(signplySDKParams: signplySDKParams, on: self, successCallback: { url in
        // url of the signed document
        print("signedDocumentSuccess")
    }, rejectCallback: { reason in
        print(reason)
    }, errorCallback: {error in
        print(error ?? "")
        print("signedDocumentError")
    })
```

# Data Model

**SignplySDKParams**
- licenseB64 (String): base 64 SIGNply license
- fileProperties (SignplySDKFileProperties): File properties.
- commonProperties (SignplySDKCommonProperties): Common properties.
- widget (SignplySDKWidget): Configurable signature widget
- tsp (SignplySDKTSP): Configurable TSP
- extra (SignplySDKExtra): Extra functionality in the configurable SDK
- certificate (SignplySDKCertificate): Configurable signing certificate

**SignplySDKFileProperties**
- documentPath (String): String path component of the URL of the document
- signedName (String): Name of the signed document
- password (String): Unlocks the document if it is password protected
- isShareable (Bool): Display share icon on top right screen
- author (String): Author of the signature
- reason (String): Reason for signing
- contact (String): Contact of the author or entity of the signature
- location (String): Location of the author or entity of the signature

**SignplySDKCommonProperties**
- title (String?): Display title in Navigation Bar
- requestLocation (Bool): Allows to request user location on signature
- renderDefaultIndexPage (NSNumber?): Renders page index
- reference (String): External reference for grouping purposes. Default empty.
- saveOnPrivateStorage (Bool): Allows to save the document on internal app storage. Default is false.

**SignplySDKWidget**
- widgetType (SignplySDKWidgetType): widget positioning type. Defaults Manual.
- widgetFloatText (String): indicates the string to search for within the document where the widget is embedded. Returns the first result obtained. This parameter is case sensitive.
- widgetFieldFieldname (String) - name of pre-existing field in which the widget is embedded.
- widgetManualRatio (Float) - Ratio of widget width to height for manual positioning. Default 2.5.
- widgetFixedPage (NSNumber) - Number of the page on which the widget is embedded.
- widgetFixedX (NSNumber) - Indicates the horizontal offset of the widget position from the bottom left corner of a document page.
- widgetFixedY (NSNumber) - Indicates the vertical offset of the widget position from the bottom left corner of a document page.
- widgetFloatGapY (NSNumber): Indicates the integer number of offset units (positive or negative) vertically from the bottom to the text
- widgetFloatGapX (NSNumber): Indicates the integer number of offset units (positive or negative) horizontally from the bottom to the text
- widgetCustomText [SignplySDKWidgetCustomText]: multiline text array that is embedded in the signature widget
- widgetRequestCustomText (Bool): Indicates whether the user should be prompted for a text string to embed in the signature widget at the time of signing.
- widgetWidth (Int): width of the widget
- widgetHeight (int): height of the widget
- flexible (Bool): Variable that makes parameter validations more flexible if true. Recommendation to have it to false.
- signOnAllPages (Bool): Allows to sign on all pages.

**SignplySDKTSP**
- tspActivate (Bool): Time stamp input activation
- tspURL (String): TSP url
- tspUser (String): TSP user
- tspPassword (String): TSP password

**SignplySDKExtra**
- autoOpen (Bool): Allows to display the signature canvas automatically when opening the document
- viewLastPage (Bool): If true, it is necessary for the user to view until the last page to be able to sign
- showReject (Bool): Shows the button to reject the document
- signatureColorHex (String): Color of the signature with hexadecimal representation. Defaults is #000000 (Black)
- signatureThickness (Int): Thickness of the signature.

**SignplySDKCertificate**
- signCertP12B64 (String): Signature certificate in base64. If null is sent, it takes the default value.
- signCertPassword (String): Password of the signing certificate. If null is sent, it takes the default value.
- encKeyB64 (String): Public encryption key for biometric data in base64. If null is sent, it takes the default value.

**SignplySDKWidgetCustomText**
- fontSize (Int): Font size of the custom text
- text (String): Custom text value

**SignplySDKWidgetType**
- Manual: Manual mode positioning in which the user can move the signature freely (in Objective C it takes the value 0).
- Field: Positioning that looks for a specific signature field (in Objective C it takes the value 1).
- Fixed: Fixed position in the document, it is passed a page number and a relative position within the document (in Objective C it takes the value 2).
- Float: Floating position in the document based on a text string search (in Objective C it takes the value 3)


# Annotations

For a new or existing project to add a SDK the next instructions are required:

Add the SignplySDK.xcframework and SignplySB.xcframework to section Frameworks, Libraries and Embedded content (Embed & Sign)
